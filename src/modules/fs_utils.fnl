;; modules/fs_utils.fnl
;; Provides a collection of utilities for performing some filesystem operations

;; -------------------------------------------------------------------------- ;;
;; Module definition

(local fs_utils {})

(fn fs_utils.path_exists? [path]
  "Return true if `path` exists and is accessible.
Note that this misuses os.rename in order to check `path`-- mightn't want to touch anything important with this..."
  (let [(ok? _) (os.rename path path)]
    ok?))

(fn fs_utils.dir_exists? [path]
  "Return true if `path` is a directory and is accessible."
  (fs_utils.path_exists? (.. path "/")))

;; -------------------------------------------------------------------------- ;;
;; Return module

fs_utils
