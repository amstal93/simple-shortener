-- init.lua
-- Program entry point

--------------------------------------------------------------------------------
-- Bootstrap Fennel compiler

local fennel = require( 'fennel' )
table.insert( package.loaders, fennel.make_searcher( { correlate = true } ) )

--------------------------------------------------------------------------------
-- Initialize server

----------------------------------------
-- Disable turbo's static file caching functionality if requested

if ( os.getenv( 'disable_cache' ) ) then
   _G.TURBO_STATIC_MAX = -1
end

----------------------------------------
-- Seed RNG (Lua... why...)

math.randomseed( os.time() )

----------------------------------------
-- Load server code

print( 'Initializing server...' )
require( 'server' )
