/**
 * components/LinkList.js
 *
 * @file Provides a link list component that displays a list of shortened links
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Helper functions */

/* ---------------------------------- */
/* highlightInText */

/**
 * Return `text` with the first instance of `query` wrapped in a highlighting span component
 * Returns `text` completely wrapped in a highlighting span component if `text` === `query`.
 * Or, returns `text` if no instance of `query` was found.
 *
 * @returns {string} text - The text in which to search for the query
 * @param {string} query - The query to highlight
 *
 * @returns {(m.Vnode|Array|string)} `text` with the first instance of `query` highlighted
 */
function highlightInText( text, query )
{
    const regexp = new RegExp( `${ query }`, 'i' );
    const matchIndex = text.search( regexp );
    if ( matchIndex !== -1 )
    {
        if ( query.length === text.length )
        {
            return m( 'span.query-match', text );
        }
        const before = text.substring( 0, matchIndex );
        const match = text.match( regexp );
        const after = text.substring( matchIndex + query.length );
        return [ before, m( 'span.query-match', match ), after ];
    }
    return text;
};

/* -------------------------------------------------------------------------- */
/* Component implementation */

const LinkList = {

    /* ---------------------------------- */
    /* view */

    /**
     * Called whenever the component is drawn
     *
     * @param {m.Vnode} vnode - The Vnode representing the component
     *
     * @returns {m.Vnode} - The Vnode or Vnode tree to be rendered
     */
    view: ( { attrs } ) => {
        return m( 'section.LinkList', [
            attrs.links.length === 0
                ? [ m( '.nothing-here.has-text-grey-light', [ m( 'i.fa.fa-folder-open.mr-2' ) ], 'Nothing here' ) ]
                : attrs.links.map( ( link ) => {
                    return m( 'a.panel-block', {
                        onclick: () => m.route.set( '/app/options/:linkID', { linkID: link.link_id } )
                    }, [
                        m( '.link-name', [
                            m( 'i.fa.fa-tag.has-text-grey-light.mr-2' ),
                            m( 'span', {
                                title: link.link_id
                            }, highlightInText( link.link_id, attrs.highlight ) )
                        ] ),
                        m( '.link-dest', [
                            m( 'i.fa.fa-link.has-text-grey-light.mr-2' ),
                            m( 'span', {
                                title: link.link_dest
                            }, highlightInText( link.link_dest, attrs.highlight ) )
                        ] )
                    ] )
                } )
        ] );
    }

}

/* -------------------------------------------------------------------------- */
/* Export */

export { LinkList };
