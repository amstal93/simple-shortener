/**
 * views/Auth.js
 *
 * @file Provides top-level component for auth screen
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { Auth as Controller } from '../controllers/Auth.js';

/* -------------------------------------------------------------------------- */
/* Component implementation */

const Auth = {

    /* ---------------------------------- */
    /* oninit */

    /**
     * Called when the component is initialized
     */
    oninit: () => {
        Controller.init();
    },

    /* ---------------------------------- */
    /* onremove */

    /**
     * Called before component element is removed from the DOM
     */
    onremove: () => {
        Controller.reset();
    },

    /* ---------------------------------- */
    /* view */

    /**
     * Called whenever the component is drawn
     *
     * @returns {m.Vnode} - The Vnode or Vnode tree to be rendered
     */
    view: ( { state } ) => {
        return m( 'main.Auth', [
            m( 'h1.title', 'Link shortener' ),
            m( 'h1.subtitle', window.location.hostname ),
            m( '.box', {
                class: Controller.isError ? 'shake' : null,
            }, [
                m( '.field.has-addons', [
                    m( '.control.has-icons-left', [
                        m( 'input.input', {
                            class: Controller.isError ? 'is-danger' : null,
                            type: 'password',
                            placeholder: 'Password',
                            disabled: Controller.isWaiting,
                            oncreate: ( { dom } ) => dom.focus(),
                            oninput: ( event ) => Controller.updatePassword( event.target.value ),
                            onkeydown: ( event ) => {
                                if ( !Controller.isWaiting && event.key === 'Enter' )
                                {
                                    Controller.tryLogIn( state.password );
                                }
                            }
                        } ),
                        m( 'span.icon.is-left', [
                            m( 'i.fa.fa-lock' )
                        ] )
                    ] ),
                    m( '.control', [
                        m( 'button.button.is-link.is-outlined', {
                            class: Controller.isWaiting ? 'is-loading' : null,
                            onclick: ( event ) => {
                                if ( !Controller.isWaiting )
                                {
                                    Controller.tryLogIn( state.password );
                                }
                            }
                        }, [
                            m( 'i.fa.fa-sign-in-alt' )
                        ] )
                    ] )
                ] )
            ] )
        ] );
    }

}

/* -------------------------------------------------------------------------- */
/* Export */

export { Auth };
